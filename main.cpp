/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: danmax
 *
 * Created on 28 февраля 2019 г., 20:46
 */

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <stdexcept>
#include "Matrix.h"

using namespace std;

int distanse(Matrix v) {
    int min = v.x, count;
    for (int i = 0; i < v.y; i++) {
        count = 0;
        for (int j = 0; j < v.x; j++) {
            if (v.m[i][j] == 1) {
                count++;
            }
        }
        if (count < min && count != 0) {
            min = count;
        }
    }
    return min;
}

long long fact(int n) {
    if (n < 0) {
        throw invalid_argument("invalid parametr. n < 0");
    }
    int res = 1;
    for (int i = 1; i <= n; i++) {
        res *= i;
    }
    return res;
}

long long combination(int n, int k) {
    if (n < k) {
        throw invalid_argument("invalid parametr. n >= k");
    }
    return fact(n) / (float)(fact(n - k) * fact(k));
}

int hemming(int q, int n, int d) {
    int sum = 0, t = floor((float) (d - 1) / 2);
    for (int i = 0; i <= t; i++) {
        sum += (combination(n, i) * pow(q - 1, i));
    }
    return pow(q, n) / sum;
}

int vg(int q, int n, int d) {
    int sum = 0;
    for (int i = 0; i < d; i++) {
        sum += (combination(n, i) * pow(q - 1, i));
    }
    return pow(q, n) / sum;
}

int singleton(int q, int n, int d) {
    return pow(q, n - d + 1);
}

bool NextSet(int *a, int n, int m) {
    int k = m;
    for (int i = k - 1; i >= 0; --i)
        if (a[i] < n - k + i + 1) {
            ++a[i];
            for (int j = i + 1; j < k; ++j)
                a[j] = a[j - 1] + 1;
            return true;
        }
    return false;
}

Matrix getWords(Matrix in) {
    int size = pow(2.0, in.y);
    int **arr = new int*[size];
    for (int i = 0; i < size; i++) {
        arr[i] = new int[in.x];
    }
    int *sum = new int[in.x];
    for (int i = 0; i > size; i++) {
        arr[i] = new int[in.x];
    }
    int *a, n = in.y, counter = 0;
    for (int m = 2; m < n + 1; m++) {
        for (int i = 0; i < in.x; i++) {
            sum[i] = 0;
        }
        a = new int[m];
        for (int i = 0; i < m; i++)
            a[i] = i + 1;
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < in.x; i++) {
                sum[i] = (sum[i] + in.m[a[j] - 1][i]) % 2;
            }
        }
        for (int i = 0; i < in.x; i++) {
            arr[counter][i] = sum[i];
        }
        counter++;
        if (n >= m) {
            while (NextSet(a, n, m)) {
                ;
                for (int i = 0; i < in.x; i++) {
                    sum[i] = 0;
                }
                for (int j = 0; j < m; j++) {
                    for (int i = 0; i < in.x; i++) {
                        sum[i] = (sum[i] + in.m[a[j] - 1][i]) % 2;
                    }
                }
                for (int i = 0; i < in.x; i++) {
                    arr[counter][i] = sum[i];
                }
                counter++;
            }
        }
    }
    for (int i = 0; i < in.y; i++) {
        for (int j = 0; j < in.x; j++) {
            arr[counter][j] = in.m[i][j];
        }
        counter++;
    }
    for (int i = 0; i < in.x; i++) {
        arr[counter][i] = 0;
    }
    Matrix res = Matrix(in.x, size, arr);
    for (int i = 0; i < size; i++) {
        delete [] arr[i];
    }
    delete [] sum;
    delete [] arr;
    return res;
}

int main(int argc, char** argv) {
    try {
        int n, k;
        if (argc == 1) {
            cout << "n = ";
            cin >> n;
            cout << "k = ";
            cin >> k;
            cout << endl;
        } else {
            n = atoi(argv[0]);
            k = atoi(argv[1]);
        }
        if(n <= k){
            throw invalid_argument("invalid parametr. n > k");
        }
        Matrix p = Matrix(k, k, "e").hconcat(Matrix(n - k, k, ""));
        cout << "p" << endl << p << endl;
        Matrix words = getWords(p);
        cout << "words count = " << words.y << endl << words << endl;
        int d = distanse(words);
        cout << "d = " << d << endl;
        int t = floor((float) (d - 1) / 2);
        cout << "t = " << t << endl;
        cout << "Hemming: " << words.y << " <= " << hemming(2, n, d) << endl;
        cout << "Gilbert Varshamov: " << words.y << " >= " << vg(2, n, d) << endl;
        cout << "Singleton: " << words.y << " <= " << singleton(2, n, d) << endl;
        return 0;
    } catch (exception &e) {
        cout << e.what() << endl;
        return -1;
    }
}