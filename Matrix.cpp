/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Matrix.cpp
 * Author: danmax
 * 
 * Created on 28 февраля 2019 г., 21:01
 */

#include "Matrix.h"

Matrix::Matrix(int sizex, int sizey, string mode) {
    if (mode == "e" && sizex != sizey) {
        throw invalid_argument("invalid size");
    }
    x = sizex;
    y = sizey;
    m = new int* [y];
    for (int i = 0; i < y; i++) {
        m[i] = new int[x];
    }
    for (int i = 0; i < y; i++) {
        for (int j = 0; j < x; j++) {
            m[i][j] = 0;
        }
    }
    if (mode == "e") {
        for (int i = 0; i < y; i++) {
            m[i][i] = 1;
        }
    } else {
        srand(time(0));
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                m[i][j] = (((float) rand() / RAND_MAX) < 0.75f);
            }
        }
    }
}

Matrix::Matrix(const Matrix& orig) {
    x = orig.x;
    y = orig.y;
    m = new int* [y];
    for (int i = 0; i < y; i++) {
        m[i] = new int[x];
        for (int j = 0; j < x; j++) {
            m[i][j] = orig.m[i][j];
        }
    }
}

Matrix::~Matrix() {
    for (int i = 0; i < y; i++) {
        delete [] m[i];
    }
    delete [] m;
}

Matrix::Matrix(int sizex, int sizey, int** mat) {
    x = sizex;
    y = sizey;
    m = new int* [y];
    for (int i = 0; i < y; i++) {
        m[i] = new int[x];
        for (int j = 0; j < x; j++) {
            m[i][j] = mat[i][j];
        }
    }
}

Matrix Matrix::hconcat(Matrix v) {
    if (y != v.y) {
        throw invalid_argument("invalid sizes");
    }
    int sizex = x + v.x;
    int** res = new int* [y];
    for (int i = 0; i < y; i++) {
        res[i] = new int[sizex];
    }
    int counter;
    for (int i = 0; i < y; i++) {
        counter = 0;
        for (int j = 0; j < sizex; j++) {
            if (j < x) {
                res[i][j] = m[i][j];
            } else {
                res[i][j] = v.m[i][counter];
                counter++;
            }
        }
    }
    Matrix resm = Matrix(sizex, y, res);
    for(int i = 0; i < y; i++){
        delete [] res[i];
    }
    delete [] res;
    return resm;
}

ostream& operator<<(ostream& out, const Matrix &matrix) {
    for (int i = 0; i < matrix.y; i++) {
        for (int j = 0; j < matrix.x; j++) {
            if (j != matrix.x - 1) {
                out << matrix.m[i][j] << " ";
            } else {
                out << matrix.m[i][j];
            }
        }
        out << endl;
    }
    return out;
}
