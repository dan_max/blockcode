/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Matrix.h
 * Author: danmax
 *
 * Created on 28 февраля 2019 г., 21:01
 */

#ifndef MATRIX_H
#define MATRIX_H

#include <string>
#include <stdexcept>
#include <iostream>
#include <time.h>

using namespace std;

class Matrix {
public:
    int** m;
    int x, y;
    Matrix(int sizex, int sizey, string mode);
    Matrix(const Matrix& orig);
    virtual ~Matrix();
    Matrix hconcat(Matrix v);
    Matrix(int sizex, int sizey, int** mat);
private:
};
ostream& operator<<(ostream& out, const Matrix &matrix);
#endif /* MATRIX_H */

